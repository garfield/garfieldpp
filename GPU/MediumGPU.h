#ifndef G_MEDIUMGPU_H
#define G_MEDIUMGPU_H

#ifndef __GPUCOMPILE__
#error GPU HEADER INCLUDED WITHOUT SETTING __GPUCOMPILE__
#endif

#include "GPUInterface.hh"
#include "Garfield/MagboltzInterface.hh"
#include "Garfield/Medium.hh"


#endif
